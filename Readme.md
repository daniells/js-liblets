# Javascript Utility Liblets
Just a place to stick useful .js files which do this and that.
Everything in here is [MIT licensed](http://opensource.org/licenses/MIT).

## Available

##### Math.plus.js

A small library of addons that give both arrays and the Math object extra methods for processing sets of values.

* Math.squr(num)
* Math.cube(num)
* Math.mean(x1,x2,xn),  Math.avg(x1,x2,xn)
* Math.median(x1,x2,xn)
* Math.mode(x1,x2,xn)
* Math.range(x1,x2,xn)
* Math.rms(x1,x2,xn)
* Math.lineLen(x1,y1,x2,y2)
* Math.irandom(min, max)
* Math.frandom(min,max)
* [].mean(), [].avg()
* [].median()
* [].mode()
* [].range()
* [].rms()
* [x1,y1,x2,y2].lineLen()  |  [[x1,y1],[x2,y2],[xn,yn]].lineLen()

## In Progress

##### CookieJar.js

An simple, object-oriented interface to document.cookie

##### WallTime.js

Date() addons that derive useful information based on calendar and clock times.

* Calculate y/m/w/d/h/m/s between two dates
* Calculate y/m/w/d/h/m/s between any two given clock times
* Calculate wall time at y/m/w/d/h/m/s after/before a given wall time
* Calculate number of days in any given month of any given year

