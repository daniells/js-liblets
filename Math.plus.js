/*      
 *      Math.plus()
 *    
 *  A small library of addons that give both arrays and the Math object extra methods for processing sets of values.
 *
 *  Math.squr(num)
 *  Math.cube(num)
 *  Math.mean(x1, x2, xn) Math.avg(x1,x2,xn)  |   [].mean() [].avg()
 *  Math.median(x1, x2, xn)                   |   [].median()
 *  Math.mode(x1, x2, xn)                     |   [].mode()
 *  Math.range(x1, x2, xn)                    |   [].range()
 *  Math.rms(x1, x2, xn)                      |   [].rms()
 *  Math.lineLen(x1,y1, x2,y2, xn,yn)         |   [x1,y1,x2,y2,xn,yn].lineLen()  |  [[x1,y1],[x2,y2],[xn,yn]].lineLen()
 *  Math.irandom(min, max)
 *  Math.frandom(min, max)
 *
 *
 *  If there is a problem with arguments to these methods they return false.
 *
 *  Math methods can also be passed an array. 
 * 
 *  The .mode() methods return null if the set has no mode. 
 * 
 *  If Math.rrandom() is called with no arguments it behaves like the normal Math.random()
 *  
 */     

// Square
Math.sqr = function(x){
    if(x.constructor === Number)
        return Math.pow(x,2);
    else return false;
}

// Cube
Math.cube = function(x){
    if(x.constructor === Number)
        return Math.pow(x,3);
    else return false;
}

// Average / Mean
Math.mean = function(){
    if(arguments[0].constructor === Number){    
        var sum = 0, reals = Array.prototype.slice.call(arguments).filter(function(elem){return elem.constructor === Number;});    
        if(reals.length){
            for(var i = 0; i != reals.length; i++){
                sum += reals[i];
            }
            return sum/reals.length;
        }
    }
    else if(arguments[0].constructor === Array){
        return arguments[0].mean();
    }
    else return false;
}; 
Math.avg = Math.mean;
Array.prototype.mean = function(){
    var sum = 0, reals = this.filter(function(elem){return elem.constructor === Number;});    
    if(reals.length){
        for(var i = 0; i != reals.length; i++){
            sum += reals[i];
        }
        return sum/reals.length;
    }
    else return false;
}
Array.prototype.avg = Array.prototype.mean;

// Median
Math.median = function(array){
    if(arguments[0].constructor === Number){ 
        reals = Array.prototype.slice.call(arguments).filter(function(elem){return elem.constructor === Number;}).sort(function(a, b) {return a - b;});
        if(reals.length){
            if(reals.length % 2 == 0)
                return (reals[reals.length/2-1] + reals[reals.length/2]) /2 ;
            else
                return reals[Math.ceil(reals.length/2-1)];
        }
        else return false;
    }
    else if(arguments[0].constructor === Array)
        return arguments[0].median();
    else return false;
}; 
Array.prototype.median = function(){
    reals = this.filter(function(elem){return elem.constructor === Number;}).sort(function(a, b) {return a - b;});
    if(reals.length){
        if(reals.length % 2 === 0)
            return (reals[reals.length/2-1] + reals[reals.length/2]) /2 ;
        else
            return reals[Math.ceil(reals.length/2-1)];
    }
    else return false;
}; 

// Mode
// Returns null if a set has no mode.  Null is falsy, but null !== false so you can distinguish the difference.
Math.mode = function(){
    if(arguments[0].constructor === Number){ 
        var reals = Array.prototype.slice.call(arguments).filter(function(elem){return elem.constructor === Number;});
        if(reals.length){
            var mode = String(reals[0]), bins = {};
            reals.forEach(
                function(elem){
                    bins[elem.toString()] = bins[elem.toString()] === undefined ? 1 : bins[elem.toString()] + 1 ;
                }
            );
            for(var key in bins){
                mode = bins[key] > bins[mode] ? key : mode;
            }
            if(bins[mode] > 1)
                return Number(mode);
            else return null;
        }
    }
    else if(arguments[0].constructor === Array)
        return arguments[0].mode();
    else return false;
}; 
Array.prototype.mode = function(){
    var reals = this.filter(function(elem){return elem.constructor === Number;});
    if(reals.length){
        var mode = String(reals[0]), bins = {};
        reals.forEach(
            function(elem){
                bins[elem.toString()] = bins[elem.toString()] === undefined ? 1 : bins[elem.toString()] + 1 ;
            }
        );
        for(var key in bins){
            mode = bins[key] > bins[mode] ? key : mode;
        }
        if(bins[mode] > 1)
            return Number(mode);
        else return null;
    }
    else return false;
}; 

// Range
Math.range = function(){
    if(arguments[0].constructor === Number){ 
        reals = Array.prototype.slice.call(arguments).filter(function(elem){return elem.constructor === Number;}).sort(function(a, b) {return a - b;});
        if(reals.length)
            return reals[reals.length-1] - reals[0];
        else return false;
    }
    else if(arguments[0].constructor === Array)
        return arguments[0].range();
    else return false;
}; 
Array.prototype.range = function(){
    reals = this.filter(function(elem){return elem.constructor === Number;}).sort(function(a, b) {return a - b;});
    if(reals.length){
        return reals[reals.length-1] - reals[0];
    }
    else return false;
}; 

// Root Mean Square
Math.rms = function(){
    if(arguments[0].constructor === Number){    
        var squaresum = 0, reals = Array.prototype.slice.call(arguments).filter(function(elem){return elem.constructor === Number;});    
        if(reals.length){
            reals.forEach(
                function(elem){
                    squaresum += Math.pow(elem,2);
                }
            );
            return Math.sqrt(squaresum/reals.length);
        }
        else return false;
    }
    else if(arguments[0].constructor === Array)
        return arguments[0].rms();
    else return false;
}; 
Array.prototype.rms = function(){
    var squaresum = 0, reals = this.filter(function(elem){return elem.constructor === Number;});    
    if(reals.length){
        reals.forEach(
            function(elem){
                squaresum += Math.pow(elem,2);
            }
        );
        return Math.sqrt(squaresum/reals.length);
    }
    else return false;
}; 

// Length of a line segment or polyline 
Math.lineLen = function(){
    if(arguments[0].constructor === Array)
        return arguments[0].lineLen();
    else if(arguments[0].constructor === Number){
        var reals = Array.prototype.slice.call(arguments).filter(function(elem){return elem.constructor === Number;});  
        if(!(reals.length%2)){ 
            var sum = 0;
            for(var i=0; i<reals.length-2; i+=2){
                sum +=  ( Math.sqrt( Math.pow( (reals[i+2] - reals[i]),2) + Math.pow( (reals[i+3] - reals[i+1]) ,2) ) );
            }
            return sum;
        }
        else{console.log("!(!(reals.length%2) && reals.length > 3)"); return false;}
    }
    else{console.log(arguments[0].consuctor); return false;}
}
Array.prototype.lineLen = function(){
    if(this[0].constructor === Number){
        var reals = this.filter(function(elem){return elem.constructor === Number;});  
        if(!(reals.length%2)){ 
            var sum = 0;
            for(var i=0; i<reals.length-2; i+=2){
                sum +=  ( Math.sqrt( Math.pow( (reals[i+2] - reals[i]),2) + Math.pow( (reals[i+3] - reals[i+1]) ,2) ) );
            }
            return sum;
        }
        else return false;
    }
    else if(this[0].constructor === Array){
        var pairs = this.filter(function(elem){return elem.constructor === Array;});
        var sum = 0;
        for(var i=0; i < pairs.length; i++){
            if(pairs[i+1]){
                var reals_a = pairs[i].filter(function(elem){return elem.constructor === Number;});
                var reals_b = pairs[i+1].filter(function(elem){return elem.constructor === Number;});
                if(reals_a.length === 2 && reals_b.length === 2)
                    sum +=  ( Math.sqrt( Math.pow( (reals_b[0] - reals_a[0]),2) + Math.pow( (reals_b[1] - reals_a[1]) ,2) ) );
            }  
        }
        return sum;
    }
    else return false;
}

// Random integer between a min and max.  If no argument it returns a random decimal between 0 and 1.
//     the way this is written now, the random int will never fall on the max.  this needs to change.
Math.irandom = function(min, max) {
    if(arguments.length === 0)
        return Math.random();
    if(min.constructor === Number && max.constructor === Number && arguments.length === 2){
        var rand = Math.random();
        return Math.floor(Math.random() * (max - min)) + min;   
    }
    else return false;
}
// Random float between a min and max.  If no argument it returns a random decimal between 0 and 1.
Math.frandom = function(min, max) {
    if(arguments.length === 0)
        return Math.random();
    if(min.constructor === Number && max.constructor === Number && arguments.length === 2)
        return Math.random() * (max - min) + min;
    else return false;
}

